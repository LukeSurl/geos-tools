# README #

This repository is for small utilities for use with GEOS-Chem input and output. Each should be a standalone utility, though if several .

This repository is likely to be mostly, if not entirely, Python scripts.

Code is written primarily for use on the JASMIN computer, though it should be adaptable for other situations.

Each file should be sufficiently commented to allow for use without additional help. 

### Management ###

* Luke Surl L.Surl@ed.ac.uk

### Copyright ###
The contents of this repository are public domain.
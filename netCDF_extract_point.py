#!/usr/bin/env python
"""netCDF_extract_point.py - a routine to extract data from GEOS-FP input files as text"""

from netCDF4 import Dataset
from datetime import datetime as dt
from datetime import timedelta as td

#Writen by Luke Surl.
#L.Surl@ed.ac.uk
#CC0 (Public Domain)

#To use:
#1) Edit the inputs below accordingly.
#2) chmod +x netCDF_extract_point.py
#3) ./netCDF_extract_point.py

#Currently this is only tested for "A1" files and 2x2.5 resolution.
#Files with 3D data will likely require modification to this script 

#===USER INPUTS===
#Point for which data should be extracted
inspect_lat = -15.
inspect_lon = 145.

#Dates between which data should be extracted
start_date = dt(2013,1,1,0,0,0)
end_date = dt(2014,12,31,23,59,59)

#temporal resolution of GEOS_FP files, hours
temp_res_hours = 1

#variables to extract
var_to_extract = ['PBLH','LAI','PARDF','PARDR','T2M'] 

#Top-level input directory
#<resolution> will be replaced by the appropriate text
in_dir = '/group_workspaces/jasmin/geoschem/ExtData/GEOS_<resolution>/GEOS_FP/'

#Location to save output to
outfile_string = "/home/users/lsurl/temp/site_PBLH.csv"

#==Other inputs: changing these has not been tested!===
#resolution of the GEOS_FP files.
lat_resolution = 2.
lon_resolution = 2.5

#The GEOS_FP type file
file_type  ="A1" 


#===Processing===
#=processing settings=

#resolution text
if lat_resolution == 2. and lon_resolution == 2.5:
    in_dir=in_dir.replace("<resolution>","2x2.5")
    res_text = "2x25"
elif lat_resolution == 4. and lon_resolution == 5.:
    in_dir=in_dir.replace("<resolution>","4x5")
    res_text = "4x5"
else:
    print "lat_resolution of %f and/or lon_resolution of %f not valid"
    print "Must be either 2. and 2.5 (2x2.5), or 4. and 5. (4x5)"
    print "Defaulting to 2x2.5"
    lat_resolution = 2.
    lon_resolution = 2.5
    in_dir=in_dir.replace("<resolution>","2x2.5")
    res_text = "2x25"
    _ = raw_input("Press enter to continue")
    
#Find co-ordinates of location in x/y
x = int((inspect_lon - -180.)/lon_resolution)
y = int((inspect_lat - -90.) /lat_resolution)

#number of times per day
times_per_day = 24/temp_res_hours

#=open file=
outfile= open(outfile_string,"w")

#=write headers=
outfile.write("UTC TIME")
outfile.write(",")
outfile.write("LAT")
outfile.write(",")
outfile.write("LON")
for var in var_to_extract:
    outfile.write(",")
    outfile.write(var)
outfile.write("\n")

#=loop through times=
date_clock = start_date + td(minutes=30) #there's a 30 minute offset for some reason
while date_clock < end_date:
    year = date_clock.year
    month = date_clock.month
    if month < 10:
        month_padded = "0"+str(month)
    else:
        month_padded = str(month)
    day = date_clock.day
    if day < 10:
        day_padded = "0"+str(day)
    else:
        day_padded = str(day)
    #Open netCDF file for this day
    #Assumes one file per day
    ncfile_string = '%s/%i/%s/GEOSFP.%i%s%s.%s.%s.nc' %(in_dir,year,month_padded,year,month_padded,day_padded,file_type,res_text)
    print "opening %s" %ncfile_string
    ncfile = Dataset(ncfile_string,'r')
    
    #loop through times in day   
    for h in range(0,times_per_day-1):
        #print "processing %s" %date_clock.strftime("%Y-%m-%d %H:%M:%S")
        outfile.write(date_clock.strftime("%Y-%m-%d %H:%M:%S"))
        outfile.write(",")
        outfile.write(str(ncfile.variables['lat'][y])) #latitude of cell
        outfile.write(",")
        outfile.write(str(ncfile.variables['lon'][x])) #longitude of cell
        for var in var_to_extract:
            outfile.write(",")
            outfile.write(str(ncfile.variables[var][h][y][x])) #data  
        outfile.write("\n")
        date_clock += td(hours=temp_res_hours) #go to next time
    ncfile.close()
    
outfile.close()
